﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts
{
    public class EndScene : MonoBehaviour
    {
        public GameObject text2;
        private void Start()
        {
            changeText();
        }
        void changeText()
        {
            text2.GetComponent<TextMeshProUGUI>().text = GameManager.wL;
        }

    }
}

