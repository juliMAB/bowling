﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MySceneManager : MonoBehaviour
{
    public static MySceneManager inst;

    private void Awake()
    {
        if (MySceneManager.inst == null)
        {
            MySceneManager.inst = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void PlayGame()
    {
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("GAME");
    }

    public void ExitGame()
    {
        Cursor.lockState = CursorLockMode.None;
        Debug.Log("QUIT!");
        Application.Quit();
    }
    public void BackToMenu()
    {
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("MENU");
    }
    public void PlayGame2()
    {
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("GAME2");
    }

    public void GoToEnd()
    {
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("END");
    }
}
