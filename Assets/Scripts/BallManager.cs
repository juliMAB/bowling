﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class BallManager: MonoBehaviour
    {
        public float minForce = 600f;
        [SerializeField]
        private float movementSpeed = 50f;
        private Transform myT;
        public float chargeForce=0f;
        public float maxForce = 1000f;
        [NonSerialized]
        public bool made = false;
        private GameObject spawn;
        private GameObject manager;
        private Rigidbody rb;
        [SerializeField] MeshRenderer flechaMeshRenderer;
        public float multiplicador;
        public float angularVelocity;
        public float maxAngle;
        public float minAngle;
        private float actualAngle;
        private bool direction;
        public float influenciaDeFlecha;
        public CamaraManager cam;
        public bool OnZona= false;
        private void Awake()
        {   
            cam = GameObject.Find("Camera").GetComponent<CamaraManager>();
            rb = GetComponent<Rigidbody>();
            myT = transform;
            spawn = GameObject.FindGameObjectWithTag("spanwBall");
            manager = GameObject.FindGameObjectWithTag("Manager");
        }

        private void Start()
        {
            chargeForce = minForce;
            actualAngle = 0;
        }
        private void Update()
        {
            Move();
            ChargeShot();
            ChangeAngle();
            Shot();
        }

        private void Move()
        {
            if (made) return;
            if (Input.GetAxis("Horizontal") != 0)
            {
                myT.position += new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * movementSpeed, 0, 0);
            }

        }

        private void ChargeShot()
        {
            if (made) return;
            if (Input.GetKey((KeyCode.UpArrow)))
            {
                chargeForce += maxForce / 100;
                if (chargeForce >= maxForce)
                {
                    chargeForce = maxForce;
                }
            }

            if (Input.GetKey((KeyCode.DownArrow)))
            {
                chargeForce -= maxForce / 100;
                if (chargeForce <= minForce)
                {
                    chargeForce = minForce;
                }
            }
        }

        private void Shot()
        {
            if (made) return;
            if (!Input.GetKeyUp((KeyCode.Space))) return;
            if (chargeForce == 0) return;
            GameObject.Find("flecha").GetComponent<MeshRenderer>().enabled = false;
            rb.velocity = new Vector3(((90-transform.rotation.eulerAngles.y) / influenciaDeFlecha) * chargeForce * multiplicador, 0, chargeForce * multiplicador);
            rb.constraints = RigidbodyConstraints.None;
            made = true;
        }
        private void ChangeAngle()
        {
            actualAngle = angularVelocity * Time.deltaTime;
            if (made) return;
            if (transform.rotation.eulerAngles.y<=minAngle|| transform.rotation.eulerAngles.y >= maxAngle)
            {
                direction = !direction;
                if (transform.rotation.eulerAngles.y >= maxAngle)
                {
                    transform.rotation = Quaternion.Euler(0, maxAngle, 0);
                }
                if (transform.rotation.eulerAngles.y <= minAngle)
                {
                    transform.rotation = Quaternion.Euler(0, minAngle, 0);
                }
            }
            if (direction)
            {
                transform.Rotate(0,actualAngle,0);
            }
            else
            {
                transform.Rotate(0, -actualAngle, 0);
            }
        }
        public void Restart()
        {
            gameObject.transform.position = spawn.transform.position;
            transform.rotation = spawn.transform.rotation;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.constraints = RigidbodyConstraints.FreezePositionY;
            made = false;
            flechaMeshRenderer.enabled = true;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag=="spawnZona")
            {
                OnZona = true;
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "spawnZona")
            {
                OnZona = false;
               
            }
            Restart();
            manager.GetComponent<GameManager>().lifes--;

        }
    }
}
