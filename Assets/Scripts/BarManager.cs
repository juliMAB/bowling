﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class BarManager : MonoBehaviour
    {
        private Slider sl;
        private BallManager ballManager;

        void Awake()
        {
            sl = GetComponent<Slider>();
            ballManager = GameObject.FindWithTag("ball").GetComponentInChildren<BallManager>();
        }

        void Start()
        {
            sl.maxValue = ballManager.maxForce;
            sl.minValue = ballManager.minForce;
        }
        void Update()
        {
            sl.value = ballManager.chargeForce;
        }
    }
}