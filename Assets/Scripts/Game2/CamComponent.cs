﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using Random = System.Random;

public class CamComponent : MonoBehaviour
{
    private Camera cam;

    public GameObject objetoAInstanciar = null;

	public float mouseSensitivity = 100f;

    public Transform playerBody;

    private float xRotation = 0f;

    public short miniPinesCuantity = 3;

    public float radio;

    public short forceMinipines;
   
    private Ray ray;

    public float timeToDestroy;

    public float torqueForce;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
    }
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        Vector3 mousePos = Input.mousePosition;
        ray = cam.ScreenPointToRay(mousePos);
        Debug.DrawRay(ray.origin, ray.direction * 50, Color.yellow);
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 200))
            {
                if (objetoAInstanciar!=null)
                {
                    GameObject go = Instantiate(objetoAInstanciar, hit.point += Vector3.up, Quaternion.identity);
                    Destroy(go, timeToDestroy * go.transform.localScale.x);
                }
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 200))
            {
                if (hit.transform.tag == "Pine")
                {
                    float angle = 360 / miniPinesCuantity;
                    for (int i = 0; i < miniPinesCuantity; i++)
                    {
                        Vector3 pos = hit.transform.position  + new Vector3( radio * Mathf.Cos(i* Mathf.Deg2Rad * angle),hit.transform.position.y+hit.transform.localScale.y/2, radio * Mathf.Sin(i* Mathf.Deg2Rad * angle));
                        if (objetoAInstanciar!=null)
                        {
                            GameObject go = Instantiate(hit.transform.gameObject, pos, Quaternion.identity);
                            go.transform.localScale -= go.transform.localScale / 5;
                            hit.transform.localScale -= hit.transform.localScale /10;
                            go.GetComponent<Rigidbody>().AddForce((go.transform.position-hit.transform.position) * forceMinipines,ForceMode.Impulse);
                            Vector3 v3 = new Vector3(go.transform.position.x - hit.transform.position.x,
                                go.transform.position.y - hit.transform.position.y,
                                go.transform.position.z - hit.transform.position.z);
                            go.GetComponent<Rigidbody>().angularVelocity = v3*torqueForce;
                            Destroy(go, timeToDestroy * go.transform.localScale.x);
                        }
                    }
                }
                if (hit.transform.tag != "Pine")
                {
                    MySceneManager.inst.GoToEnd();
                }
            }
        }
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up*mouseX);
    }
}
