﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class PineManager : MonoBehaviour
    {
        public class Data
        {
            public short Id { get;  set; }
            public bool fall { get; set; }
        }
        private Data data = new Data();
        private Vector3 firstPosition;
        private Quaternion firstRotation;
        private Rigidbody rb;
        private MeshCollider mc;
        private MeshRenderer mr;

        void Awake()
        {
            rb = GetComponent<Rigidbody>();
            mc = GetComponent<MeshCollider>();
            mr = GetComponentInChildren<MeshRenderer>();
        }
        void Start()
        {
            var transform1 = transform;
            firstRotation = transform1.rotation;
            firstPosition = transform1.position;
            
        }
        void Update()
        {
            //IsFall();
            IsMoving();
        }

        private bool IsMoving()
        {
            var velocity = rb.velocity;
            var anglar = rb.angularVelocity;
            return Mathf.Abs(velocity.x) + Mathf.Abs(velocity.y) + Mathf.Abs(velocity.z) != 0f;
        }

        public bool IsRotate()
        {
            var rotation = transform.rotation;
            if (Mathf.Abs(rotation.x) >= 0.1f || Mathf.Abs(rotation.z) >= 0.1f)
            {
                rb.constraints = RigidbodyConstraints.None;
                return true;
            }

            return false;
        }
        public bool IsFall()
        {
            if (data.fall) return false ;
            if (IsMoving() || !IsRotate()) return false;
            data.fall = true;
            mc.enabled = false;
            mr.enabled = false;
            return true;
        }

        public void FixedUpdate()
        {
            if (IsMoving())
            {
                if (rb.velocity.x<0.3f)
                {
                    rb.velocity.Set(0,0,0);
                }
            }
        }

        public void Reset()
        {
            rb.rotation.eulerAngles.Set(0,0,0);
            rb.position = Vector3.zero;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            mc.enabled = true;
            mr.enabled = true;
            var transform1 = transform;
            transform1.position = firstPosition;
            transform1.rotation = firstRotation;
            data.fall = false;
        }

        private void OnTriggerExit()
        {
            Reset();
            data.fall = true;
            mc.enabled = false;
            mr.enabled = false;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag=="ball")
            {
                rb.constraints = RigidbodyConstraints.None;
            }
            
        }

        //invoke funcion
    }
}

