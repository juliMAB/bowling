﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterManager : MonoBehaviour
{
    public float horizontalMove;
    public float verticalMove;
    public float playerSpeed;
    public float rotationSpeed;
    private Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {

        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");
        transform.position +=transform.forward*(playerSpeed*Time.deltaTime* verticalMove);
        transform.Rotate(new Vector3( 0, horizontalMove, 0) * (rotationSpeed * Time.deltaTime));
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("floor"))
        {
           rb.constraints = RigidbodyConstraints.FreezePositionY;
           //rb.constraints = RigidbodyConstraints.FreezeRotationY;
        }
    }
}
