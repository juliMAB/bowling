﻿using UnityEngine;

namespace Assets.Scripts
{
    public class DestroyerBall : MonoBehaviour
    {
        private GameObject go;

        private GameObject spawn;

        private GameObject manager;
        // Start is called before the first frame update
        void Awake()
        {
            go = GameObject.FindGameObjectWithTag("ball");
            spawn = GameObject.FindGameObjectWithTag("spanwBall");
            manager = GameObject.FindGameObjectWithTag("Manager");
        }
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag=="ball")
            {
                other.GetComponent<BallManager>().Restart();
                //manager.GetComponent<GameManager>().lifes--;
            }
        
        }
    }
}
