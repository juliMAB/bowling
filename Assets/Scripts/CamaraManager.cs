﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.Scripts
{
    public class CamaraManager : MonoBehaviour
    {
        private Vector3 firstPosition;
        public GameObject ball;
        private void Awake()
        {
            firstPosition = transform.position;
        }
        private void Update()
        {
            if (ball.GetComponent<BallManager>().made)
            {
                if (ball.GetComponent<BallManager>().OnZona) return;
                Follow();
               
            }
            else
            {
                DontFollow();
            }
            
    


        }
        private void Follow()
        {
            transform.rotation.SetLookRotation(ball.transform.position);
            transform.position = ball.transform.position + Vector3.back * 20+ Vector3.up * 20;

        }
        private void DontFollow()
        {

            transform.position = firstPosition;
        }
    }

}
