﻿
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour
    {
        private static short PineCuantity=15;
        [System.NonSerialized]
        public short lifes = 3;
        private short pinesLeft = 15;
        private List<PineManager> listDataPines = new List<PineManager>();
        [SerializeField]
        public GameObject pinoGo;
        static public int score;

        public float pinesDistanceZ;
        public float pinesDistanceX;
        public TextMeshProUGUI text;
        public TextMeshProUGUI text2;
        static public string wL;
        public float posibleP;
        private void Start()
        {
            MakeTheVpines();
        }

        private void Update()
        {
            UpdateFalls();
            if (YouWin())
            {
                wL = SeeEnd("Win");
               MySceneManager.inst.GoToEnd();
            }
            if (YouLose())
            {
                wL = SeeEnd("Lose");
                MySceneManager.inst.GoToEnd();
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                MakePineRandom();
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                ResetAll();
            }
        }
        private void MakeTheVpines()
        {
            Transform spawn = GameObject.FindWithTag("PinesSpawn").transform;
            Vector3 pos = spawn.position;
            short indexAux=0;
            for (int i = 0; i < 5; i++)
            {
                pos.z += pinesDistanceZ;
                for (int j = 0; j <= i; j++)
                {
                    GameObject go = Instantiate(pinoGo, spawn.position, Quaternion.identity);
                    go.transform.localScale *= 5;
                    go.transform.parent = spawn;
                    if (j==0)
                    {
                        pos.x += pinesDistanceX/2;
                    }
                    else
                    {
                        pos.x += -pinesDistanceX;
                    }
                    go.transform.position = pos;
                    if (j==i)
                    {
                        for (int k = j; k > 0; k--)
                        {
                            pos.x+= pinesDistanceX;
                        }
                        
                    }
                    go.name = "Pino " + (indexAux+1);
                    listDataPines.Add(go.GetComponent<PineManager>());
                    indexAux++;
                }
            }
        }
        private void MakePineRandom()
        {
            Transform piso = GameObject.Find("Floor").transform;
            Vector3 pos = piso.position;

            pos.x += Random.Range(-piso.localScale.z*2, piso.localScale.z*2);
            pos.y += pinoGo.transform.localScale.y*2;
            pos.z += Random.Range(-piso.localScale.x*posibleP, piso.localScale.x*posibleP);
            GameObject go = Instantiate(pinoGo, pos, Quaternion.identity);
            go.transform.parent = null;
            go.name = "Pino Random";
            listDataPines.Add(go.GetComponent<PineManager>());   
        }


        private void UpdateFalls()
        {
            for (int i = 0; i < listDataPines.Count; i++)
            {
                if (listDataPines[i].IsFall())
                {
                    pinesLeft--;
                    score += 10;
                }
            }
            text.text = "Force:\nLives: " + lifes + "\nPines: " + pinesLeft +
                "\nScore: " + score;
        }

        private bool YouWin()
        {
            return pinesLeft==0;
        }

        private bool YouLose()
        {
            return lifes == 0;
        }

        private void ResetAll()
        {
            GameObject.FindWithTag("ball").GetComponent<BallManager>().Restart();
            lifes = 3;
            for (int i = 0; i < PineCuantity; i++)
            {
                listDataPines[i].Reset();
            }
            text2.text = "";
        }

        private string SeeEnd(string st)
        {
            return "YOU " + st;
        }
    }
}
